package apps.abhibhardwaj.com.doctriod.patient.nearby;


import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import apps.abhibhardwaj.com.doctriod.patient.R;
import apps.abhibhardwaj.com.doctriod.patient.models.NearbyPlacesModel;
import apps.abhibhardwaj.com.doctriod.patient.models.Result;
import apps.abhibhardwaj.com.doctriod.patient.models.User;
import apps.abhibhardwaj.com.doctriod.patient.others.AboutAppActivity;
import apps.abhibhardwaj.com.doctriod.patient.others.Utils;
import apps.abhibhardwaj.com.doctriod.patient.profile.ProfileActivity;
import apps.abhibhardwaj.com.doctriod.patient.startup.LoginActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.content.DialogInterface;

public class NearbyActivity extends AppCompatActivity implements OnClickListener,
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback {

    private static final int LOCATION_REQUEST_CODE = 101;
    private static final String API_KEY = "AIzaSyB0mzGO2Yn9fl8RCzWFFCdp8_6zfz9Rerc";
    private static int TOGGLE_FAB = 1;
    private Toolbar toolbar;
    private ImageView ivBack;
    private TextView tvToolbarTitle;
    private TextView tvTitle, tvUserName, tvUserEmail;
    private ImageView ivMenu, ivClose;
    private CircleImageView ivUserProfile;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private GridView gridView;
    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private GoogleMap gMap;
    private Location currentLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private SupportMapFragment mapFragment;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageButton btnToggleBottomSheet;
    private TextView tvSelectedStatus;
    private FloatingActionButton fabToggleMapList;
    private LinearLayout btnHospital, btnDentist, btnDoctor, btnPharmacy;
    private NearPlaceInterface service;
    private ProgressDialog dialog;
    private ArrayList<Result> placesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initToolbar();
        initFireBase();
        initNavHeader();
        initBottomSheet();
        initViews();
        addClickListeners();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat
                .checkSelfPermission(NearbyActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(NearbyActivity.this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat
                    .requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            LOCATION_REQUEST_CODE);
            return;
        }
        fetchLastLocation();
    }

    private void initFireBase() {
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        db = FirebaseFirestore.getInstance();
    }
    private void addClickListeners() {
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initViews() {
        ivBack = findViewById(R.id.iv_menu);
        ivBack.setOnClickListener(this);
        navigationView = findViewById(R.id.navigation_view);
        tvUserName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
        tvUserEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);
        drawerLayout = findViewById(R.id.drawer_layout);
        fabToggleMapList = findViewById(R.id.nearby_fab_map_list);
        fabToggleMapList.setOnClickListener(this);
        dialog = new ProgressDialog(this);
        placesList = new ArrayList<Result>();
        ivUserProfile = navigationView.getHeaderView(0).findViewById(R.id.nav_profile_image);
        recyclerView = findViewById(R.id.nearby_recycler_view);
    }
    private void initNavHeader() {

        db.collection("users").document(currentUser.getUid()).get().addOnSuccessListener(
                new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User user = documentSnapshot.toObject(User.class);
                        tvUserName.setText(user.getFullName());
                        tvUserEmail.setText(user.getEmail());

                        Picasso.get().load(user.getProfileImageURL()).into(ivUserProfile);

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Utils.makeToast(NearbyActivity.this, e.getMessage());
            }
        });
    }

    private void initBottomSheet() {
        View bottomSheetView = findViewById(R.id.nearby_bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setHideable(false);

        btnDoctor = bottomSheetView.findViewById(R.id.btm_list_iv_parking);
        btnDentist = bottomSheetView.findViewById(R.id.btm_list_iv_repair);
        btnHospital = bottomSheetView.findViewById(R.id.btm_list_iv_rental);
        btnPharmacy = bottomSheetView.findViewById(R.id.btm_list_iv_stand);
        btnToggleBottomSheet = bottomSheetView.findViewById(R.id.btm_sheet_btn_toggle);
        tvSelectedStatus = bottomSheetView.findViewById(R.id.btm_sheet_tv_status);

        btnDoctor.setOnClickListener(this);
        btnDentist.setOnClickListener(this);
        btnHospital.setOnClickListener(this);
        btnPharmacy.setOnClickListener(this);
        btnToggleBottomSheet.setOnClickListener(this);
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.nearby_tool_bar);
        tvToolbarTitle = findViewById(R.id.nearby_toolbar_tv_title);
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Nearby");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    private void fetchLastLocation() {
        @SuppressLint("MissingPermission") Task<Location> task = fusedLocationProviderClient
                .getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    Toast.makeText(NearbyActivity.this,
                            currentLocation.getLatitude() + " " + currentLocation.getLongitude(),
                            Toast.LENGTH_SHORT).show();
                    mapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.frag_map);
                    mapFragment.getMapAsync(NearbyActivity.this);
                } else {
                    Toast.makeText(NearbyActivity.this, "No Location recorded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        gMap.getUiSettings().setZoomControlsEnabled(true);
        gMap.setBuildingsEnabled(true);
        gMap.setMyLocationEnabled(true);

        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        showMarker(latLng, "Current Location");
    }

    private void showMarker(LatLng latLng, String title) {
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title);
        gMap.addMarker(markerOptions);
        gMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                        14));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResult) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResult.length > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                } else {
                    Toast.makeText(NearbyActivity.this, "Location permission missing", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
        }
    }


    private void showNearbyTaxiStand() {
        tvSelectedStatus.setText("Showing Nearby Taxi Stand");
        fetchPlacesFromAPI("taxi_stand");
    }

    private void showNearbyCarRepair() {
        tvSelectedStatus.setText("Showing Nearby Car Repair");
        fetchPlacesFromAPI("car_repair");
    }

    private void showNearbyCarRental() {
        tvSelectedStatus.setText("Showing Nearby Car Rental");
        fetchPlacesFromAPI("car_rental");
    }

    private void showNearbyParking() {
        tvSelectedStatus.setText("Showing Nearby Parking");
        fetchPlacesFromAPI("parking");
    }

    private void toggleListOrMap() {

        if (TOGGLE_FAB == 0) {
            // then show map with markers
            TOGGLE_FAB = 1;
            fabToggleMapList.setImageResource(R.drawable.ic_show_list);
            loadMapWithPlaceMarkers();

        } else {
            // show recycler view
            TOGGLE_FAB = 0;
            fabToggleMapList.setImageResource(R.drawable.ic_show_map);
            loadRecyclerViewWithPlaceDetails();
        }
    }

    private void loadMapWithPlaceMarkers() {
        recyclerView.setVisibility(View.GONE);
        mapFragment.getView().setVisibility(View.VISIBLE);

        gMap.clear();
        if (!placesList.isEmpty()) {
            for (int i = 0; i < placesList.size(); i++) {
                Result result = placesList.get(i);
                LatLng latLng = new LatLng(result.getGeometry().getLocation().getLat(),
                        result.getGeometry().getLocation().getLng());
                showMarker(latLng, result.getName(), result.getPlaceId());
            }
        }
    }

    private void showMarker(LatLng latLng, String title, final String placeId) {
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title);
        gMap.addMarker(markerOptions);
        gMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                        14));

        gMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent lauchPlaceDetailsActivity = new Intent(NearbyActivity.this, PlaceDetailsActivity.class);
                lauchPlaceDetailsActivity.putExtra("placeID", placeId);
                startActivity(lauchPlaceDetailsActivity);
            }
        });
    }

    private void loadRecyclerViewWithPlaceDetails() {
        mapFragment.getView().setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerViewAdapter = new RecyclerViewAdapter(NearbyActivity.this, currentLocation, placesList);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void fetchPlacesFromAPI(String type) {
        dialog.setMessage("Loading...");
        dialog.show();

        if (!placesList.isEmpty()) {
            placesList.clear();
        }

        service = ApiClient.getRetrofitInstance().create(NearPlaceInterface.class);

        String location = currentLocation.getLatitude() + ", " + currentLocation.getLongitude();

        Call<NearbyPlacesModel> call = service.getNearbyPlace(location, 5000, type, API_KEY);

        call.enqueue(new Callback<NearbyPlacesModel>() {
            @Override
            public void onResponse(Call<NearbyPlacesModel> call, Response<NearbyPlacesModel> response) {
                List<Result> results = response.body().getResults();
                placesList.addAll(results);
                loadMapWithPlaceMarkers();
                dialog.dismiss();

            }

            @Override
            public void onFailure(Call<NearbyPlacesModel> call, Throwable t) {
                dialog.dismiss();
                Utils.makeToast(NearbyActivity.this, t.getMessage());
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_btn_close: {
                closeNavDrawer();
                break;
            }
            case R.id.iv_menu: {
                toggleNavDrawer();
                break;
            }
            case R.id.btm_list_iv_parking: {
                showNearbyParking();
                break;
            }
            case R.id.btm_list_iv_repair: {
                showNearbyCarRepair();
                break;
            }
            case R.id.btm_list_iv_rental: {
                showNearbyCarRental();
                break;
            }
            case R.id.btm_list_iv_stand: {
                showNearbyTaxiStand();
                break;
            }
            case R.id.nearby_fab_map_list:
                toggleListOrMap();

        }
    }

    private void closeNavDrawer() {
        drawerLayout.closeDrawer(Gravity.START);
    }

    private void toggleNavDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);
        } else {
            drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                NearbyActivity.super.onBackPressed();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_item_profile: {
                startActivity(new Intent(NearbyActivity.this, ProfileActivity.class));
                break;
            }

            case R.id.nav_item_logout: {
                auth.signOut();
                startActivity(new Intent(NearbyActivity.this, LoginActivity.class));
                finish();
                break;
            }

            case R.id.nav_item_about: {
                startActivity(new Intent(NearbyActivity.this, AboutAppActivity.class));
                break;
            }

            default: {
                return false;
            }
        }
        return true;
    }

}