package apps.abhibhardwaj.com.doctriod.patient.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import apps.abhibhardwaj.com.doctriod.patient.R;
import apps.abhibhardwaj.com.doctriod.patient.models.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements OnClickListener {

  private Toolbar toolbar;
  private TextView tvToolbarTitle;
  private ImageView ivBack;

  private CircleImageView ivUserImage;
  private TextView tvFullName, tvDOB, tvEmail, tvPhone;
  private ProgressDialog progressDialog;

  private FirebaseAuth auth;
  private FirebaseUser currentUser;
  private FirebaseFirestore db;
  private User user = null;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);

    initToolbar();
    initFireBase();
    intViews();
    initActionListeners();
    fetchUserDetails();
  }
  private void initToolbar() {
    toolbar = findViewById(R.id.profile_toolbar);
    tvToolbarTitle = findViewById(R.id.profile_toolbar_tv_title);
    setSupportActionBar(toolbar);
    tvToolbarTitle.setText("Profile");
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void initFireBase() {
    auth = FirebaseAuth.getInstance();
    currentUser = auth.getCurrentUser();
    db = FirebaseFirestore.getInstance();
  }

  private void intViews() {
    ivUserImage = findViewById(R.id.profile_image);
    tvFullName = findViewById(R.id.profile_user_name);
    tvDOB = findViewById(R.id.profile_user_dob);
       tvEmail = findViewById(R.id.profile_user_email);
    tvPhone = findViewById(R.id.profile_user_phone);
    progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("Loading...");

    ivBack = findViewById(R.id.profile_toolbar_iv_back);
  }

  private void initActionListeners() {
    ivBack.setOnClickListener(this);
  }

  private void fetchUserDetails() {
    progressDialog.show();
    db.collection("users").document(currentUser.getUid()).get().addOnSuccessListener(
        new OnSuccessListener<DocumentSnapshot>() {
          @Override
          public void onSuccess(DocumentSnapshot documentSnapshot) {

            User user = documentSnapshot.toObject(User.class);
            loadUserDetails(user);
          }
        });
  }
  private void loadUserDetails(User user) {
    Picasso.get().load(user.getProfileImageURL()).into(ivUserImage);
    tvFullName.setText(user.getFullName());
    tvDOB.setText(user.getDoB());
     tvEmail.setText(user.getEmail());
    tvPhone.setText(user.getPhone());

    progressDialog.dismiss();

  }

  @Override
  public void onClick(View v) {

    switch (v.getId())
    {
      case R.id.profile_toolbar_iv_back:
      {
       onBackPressed();
        break;
      }

    }



  }
}
