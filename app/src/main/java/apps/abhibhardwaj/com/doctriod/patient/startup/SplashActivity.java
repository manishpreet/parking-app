package apps.abhibhardwaj.com.doctriod.patient.startup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import apps.abhibhardwaj.com.doctriod.patient.R;
import apps.abhibhardwaj.com.doctriod.patient.nearby.NearbyActivity;
import apps.abhibhardwaj.com.doctriod.patient.others.SharedPref;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashActivity extends AppCompatActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.splash);
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();


    if(currentUser == null)
    {
      startActivity(new Intent(SplashActivity.this, LoginActivity.class));
      finish();
    }

    else {
      startActivity(new Intent(SplashActivity.this, NearbyActivity.class));
      finish();
    }

  }
}
